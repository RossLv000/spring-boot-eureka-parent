package com.client.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@FeignClient("HelloServer")
public interface HelloClient {
	@RequestMapping(value = "/hello", method = GET)
	String hello();
}