package com.provider.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {
    @GetMapping("/say")
    public String say(){
        return "provider say...";
    }

    @GetMapping("/smile")
    public String smile(){
        return "provider smile...";
    }
}
