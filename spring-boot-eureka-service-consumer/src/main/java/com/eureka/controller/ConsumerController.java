package com.eureka.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ConsumerController {
    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/helloConsumer")
    public String helloConsumer(){
        ResponseEntity<String> entity = restTemplate.getForEntity("http://spring-boot-eureka-server/hello",String.class);
        System.out.println(entity.getBody());
        return "==>"+entity.getBody();
    }
}
