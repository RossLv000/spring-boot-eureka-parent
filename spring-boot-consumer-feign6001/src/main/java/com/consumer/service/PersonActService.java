package com.consumer.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

@Component
@FeignClient(value = "personAct",url = "localhost:7001")
public interface PersonActService {
    @GetMapping("/say")
    String say();

    @GetMapping("/smile")
    String smile();
}
