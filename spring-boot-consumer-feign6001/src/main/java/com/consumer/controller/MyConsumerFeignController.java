package com.consumer.controller;

import com.consumer.service.PersonActService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyConsumerFeignController {

    @Autowired
    private PersonActService personActService;

    @GetMapping("/doSay")
    public String doSay(){
        return "call :"+personActService.say();
    }

    @GetMapping("/doSmile")
    public String doSmile(){
        return "call :"+personActService.smile();
    }
}
