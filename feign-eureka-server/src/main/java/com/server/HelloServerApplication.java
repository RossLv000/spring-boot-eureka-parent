package com.server;

import com.client.service.HelloClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Random;

@SpringBootApplication
@EnableDiscoveryClient
@RestController
@FeignClient("HelloServer")
public class HelloServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(HelloServerApplication.class, args);
    }

    @Autowired
    DiscoveryClient discoveryClient;

    @RequestMapping("/hello")
    public String hello(){
        List<ServiceInstance> list = discoveryClient.getInstances("HelloServer");
        ServiceInstance serviceInstance = list.get(new Random().nextInt(list.size()));
        return "hello world! "+serviceInstance.getServiceId()+" :"+serviceInstance.getHost()+":"+serviceInstance.getPort();
    }
}
